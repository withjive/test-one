class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy]

  # GET /reviews
  # GET /reviews.json
  def index
    @reviews = Review.all
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
  end

  # GET /reviews/new
  def new
    @review = Review.new
  end

  # GET /reviews/1/edit
  def edit
  end

  # POST /reviews
  # POST /reviews.json
  require 'open-uri'

  def create
    url = URI(params[:review][:product_url])
    doc = open(url.to_s) { |f| Hpricot(f) }
    reviews = (doc/'#revMHLContainer .a-section > .a-section')

    created_reviews = []

    title = (doc/"#productTitle").innerHTML.strip


    reviews.map do |rev|
      body = (rev/'.a-row .a-section').innerHTML.strip
      md5 = Digest::MD5.new
      md5 << body

      rating = 0
      reviewed_by_user = 'Mr. Todo'

      # ensure reviews are unique
      unless Review.where(uuid: md5.hexdigest ).first
        created_reviews << Review.create(product_title: title,
                                         product_url: url.to_s,
                                         body: body,
                                         rating: rating,
                                         reviewed_by_user: reviewed_by_user,
                                         uuid: md5.hexdigest)
      end

    end



    if not created_reviews.empty?
      flash[:success] = "Started processing reviews for: #{url.to_s}, enjoy!"
    else
      flash[:success] = "No new reviews found"
    end

    redirect_to '/'
  end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
    respond_to do |format|
      if @review.update(review_params)
        format.html { redirect_to @review, notice: 'Review was successfully updated.' }
        format.json { render :show, status: :ok, location: @review }
      else
        format.html { render :edit }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    respond_to do |format|
      format.html { redirect_to reviews_url, notice: 'Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params[:review]
    end
end
