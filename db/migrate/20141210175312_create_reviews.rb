class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :product_title
      t.string :product_url
      t.string :reviewed_by_user
      t.integer :rating
      t.text :body
      t.timestamps
    end
  end
end
